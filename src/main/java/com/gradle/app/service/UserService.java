package com.gradle.app.service;

import org.springframework.stereotype.Service;

@Service
public class UserService {

    public String sayHello(String name) {
        String personName = "";
        if (name == null) {
            personName = "Hello unknown";
        } else {
            personName = "Hello " + name;
        }
        return personName;
    }

}
